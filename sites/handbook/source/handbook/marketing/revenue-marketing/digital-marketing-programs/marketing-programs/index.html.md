---
layout: handbook-page-toc
title: Marketing Programs Management - MPM (moving to Campaigns)
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Marketing Program Managers

[**This page is being moved to Campaigns Team handbook page.**](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/)

### Communication

See here: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#communication](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#communication)

### Priorities (current and upcoming)
`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#project-management](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/)

_Last updated 2020-07-14 by @jgragnola_

**[View Roadmap >>](https://gitlab.com/groups/gitlab-com/marketing/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=MPM%20Priority)**

**Q3 Campaigns:**

- GitOps (Launched) - @eirinipan
- VC&C (Launched) - @jennyt
- French CI (Launched) - @eirinipan
- German CI (Launched) - @ikryzeviciene
- Sales Play: [Level-Up Sales Play SMB/MM Starter](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1189) - @jgragnola @nbsmith @dpduncan
- [See all integrated campaigns,launch dates, and details](https://about.gitlab.com/handbook/marketing/campaigns/#in-progress-and-future-campaigns)

**Projects:**

- Reporting: [Integrated Campaign Dashboard](https://gitlab.com/groups/gitlab-com/-/epics/629) - @aoetama
- Reporting: [Lead to Revenue Funnel View Reporting](https://gitlab.com/groups/gitlab-com/-/epics/698) - @aoetama
- Email Marketing: [Lifecycle Email Strategy](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1040) - @nbsmith
- Email Marketing: [Use Case Campaign MarTech Architecture](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1171) - @mmmitchell
- Email Marketing: [Database Research - records not being emailed](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1224) - @jennyt
- Email Marketing: [Modular Marketo Email Marketing Master Template](https://gitlab.com/groups/gitlab-com/marketing/-/epics/759) - @nbsmith
- Email Marketing (support): [Global Email Compliance (GDPR, CASL, etc.)](https://gitlab.com/groups/gitlab-com/-/epics/415) - @nbmith, @ikryzeviciene, @eirinipan
- Email Marketing (support): [Email Marketing aOperational Needs](https://gitlab.com/groups/gitlab-com/marketing/-/epics/940) - @nbsmith @mmmitchell
- Landing Pages (support): [Marketo landing pages](https://gitlab.com/groups/gitlab-com/marketing/growth-marketing/brand-and-digital/-/epics/14) - @jgragnola
- Process/Documentation: [Workback timeline for localized campaigns](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1195) - @ikryzeviciene & @eirinipan

**Tactics:**

- Campaign Webcasts: [See campaigns webcast planning](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2300#webcast-plans-1h2021)
- Workshops: [Virtual DevOps Automation Workshops](https://gitlab.com/groups/gitlab-com/marketing/-/epics/854)

**Enablement & Training:**

- [Field Marketing Program Enablement](https://gitlab.com/groups/gitlab-com/marketing/-/epics/941) - @jennyt
- [Field Marketing Marketo/SFDC Training](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1197) - @jennyt
- [Train corporate events team on GitLab Project Management](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1196) - @nbsmith

### Key Responsibilities (in order of priority) & Quick Links

1. [Integrated Campaigns](https://about.gitlab.com/handbook/marketing/campaigns/)
    - [Active Integrated Campaigns](https://about.gitlab.com/handbook/marketing/campaigns/#active-integrated-campaigns)
    - [Upcoming and Future Integrated Campaigns](https://gitlab.com/groups/gitlab-com/marketing/-/epics/749)
    - [Past Integrated Campaigns](https://about.gitlab.com/handbook/marketing/campaigns/#past-integrated-campaigns)
    - [Campaign Planning](https://about.gitlab.com/handbook/marketing/campaigns/#campaign-planning)
1. [Marketing Agility Project](https://gitlab.com/groups/gitlab-com/-/epics/399) - related to Makreting OKRs
    - [Strengthen Our Inbound Core](https://gitlab.com/groups/gitlab-com/marketing/-/epics/896)
    - [Virtual Event Excellence](https://gitlab.com/groups/gitlab-com/-/epics/401)
    - [Marketing Efficiency Improvements aka The Boring Project](https://gitlab.com/groups/gitlab-com/-/epics/402)
    - [Marketing Project Management Simlification](https://gitlab.com/groups/gitlab-com/-/epics/403)
1. [Virtual Events](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/)
    - [Webcasts](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)
    - [Virtual Sponsorships](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#sponsored-virtual-events)
    - [Self-Service Virtual Event](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/)
1. [Emails & Nurture Programs](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture)
    - [Nurture Programs](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#email-nurture-programs)
    - [Newsletter](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#newsletter)
    - [Ad Hoc Emails](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/emails-nurture/#ad-hoc-one-time-emails---requesting-an-email)
    - [Pathfactory Target Tracks](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory/)
1. [Gated Content](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content)
    - [Resource Center (External)](https://about.gitlab.com/resources/)
    - [Internal Library of Links](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit#gid=0)
1. Supporting Other Tactics
    - [Events](https://about.gitlab.com/handbook/marketing/events/#mpm-steps-to-set-up-event-epic)
    - [Sponsored Webcasts](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-sponsored-webcast)
    - [Virtual Conferences](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-virtual-conference)
    - [Content Syndication](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication/)
    - [Direct Mail](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/direct-mail/)
    - [Surveys](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/surveys/)

## Marketing Programs Calendar

`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/)


<figure>
<iframe src="https://calendar.google.com/calendar/b/1/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FLos_Angeles&amp;src=Z2l0bGFiLmNvbV82MnA4YWM1ZmVzZ2I2OGczcjFsbnNlZjNtNEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Z2l0bGFiLmNvbV9icGp2bW03ZXJ0cnJobW1zM3I3b2pqcmt1MEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23E4C441&amp;color=%23D81B60&amp;showPrint=0&amp;showCalendars=1&amp;showTitle=0" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

## The Marketing Programs Team

`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#team-structure](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#team-structure)

# How we work together

`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#project-management](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#project-management)


## Key MPM tactics with links to processes and GoogleDocs with high level organization + timelines

#### 📌 Integrated campaigns

- [Processes defined in the Handbook](https://about.gitlab.com/handbook/marketing/campaigns/#campaign-planning)
- [GoogleDoc for workback schedule](https://docs.google.com/spreadsheets/d/1VTrWNX9qfY99b2TnrX93P39aXiRoNnChB6tduTvmysA/edit#gid=1426779885) - details all issues, DRIs, due dates based on calculations, etc. that takes into account SLAs of other teams, highly collaborative process managed by MPM DRIs working with DRIs across all of marketing.
- Example, for Q2 campaigns, we determine launch will be on June 15. The MPM puts June 15 into cell B2 and the entire workback is defined based on other team SLAs toward goal launch date.

#### 📌 Webcasts

- [Processes defined in the Handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#project-planning)
- [GoogleDoc for workback schedule](https://docs.google.com/spreadsheets/d/1VTrWNX9qfY99b2TnrX93P39aXiRoNnChB6tduTvmysA/edit#gid=1899924336) - details all issues, DRIs, due dates based on calculations, etc. that takes into account SLAs of other teams, collaborative process managed by MPM DRIs working with DRIs relevant to the webcast.

#### 📌 Gated Content

- [Processes defined in the Handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#internal-content-created-by-the-gitlab-team)
- [GoogleDoc for workback schedule](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=1060299991) - details due dates based on calculations and organizes overall calendar, collaborative process between MPMs and Content Marketing.

#### 📌 Analyst Content

- [Processes defined in the Handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#analyst-content-delivered-by-analysts)
- [GoogleDoc for workback schedule](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=1491223980) - details due dates based on calculations and organizes overall calendar, collaborative process between MPMs, Analyst Relations, and Analysts themselves.
- In addition to the setup process, we have a strict process to remove resources when we no longer have legal rights to use the assets (big project to hammer out how to do this across our website, pathfactory, landing pages, marketo, and making sure links aren't in use)

#### 📌 Events

- [Processes defined in the Handbook](https://about.gitlab.com/handbook/marketing/events/#mpm-steps-to-set-up-event-epic)
- [GoogleDoc for workback schedule](https://docs.google.com/spreadsheets/d/1mw16Ft0Wo379dT6OYingQ5A4xXTT1EjdpD6k-lgQync/edit#gid=0) - details due dates based on calculations and organizes overall calendar, collaborative process between MPMs and FMMs.

#### 📌 External (Non-GitLab Hosted) Virtual Events

- [Processes defined in the Handbook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/) - includes **sponsored webcasts** and **virtual conferences**

#### 📌 Direct Mail

- [Process defined in the Handook](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/direct-mail/)

## Timeline Guidelines

`Please note: this is being simplified and phased out. Details will be included in the project management section for Campaigns Team here:` https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#project-management

The following are some timelines to help in generating workback schedules for campaigns and tactics. These are not comprehensive, and campaigns should be planned as far in advance as possible. Bare minimum timeline is indicated, but when possible it is best to have prep done in advance of setup to reduce friction if dependencies don't meet deadlines.

**Invitation Emails:**

Dependency: _MOps target list_ must be requested through an issue, aligning to MOps timeline must be requested as part of the campaign/tactic.

Bare minimum timeline:

- T-10 BD (2 weeks) from planned send date: Issue must be opened by requeseter
- T-7 BD from planned send date: Copy due from requester (note: if copy is not provided by deadline, the send will be pushed out.)
- T-5 BD from planned send date: Test email sent to requester by MPM
- T-4 BD from planned send date: All requested changes from requester incorporated into email by MPM
- T-2 BD from planned send date: Email is set to send by MPM

**Follow Up Email:**

Dependency: for any offline campaigns/tactics (i.e. event, sponsored webcast) _MOps list upload issue_ must be requested through an issue, aligning to MOps timeline must be requested as part of the campaign/tactic.

Bare minimum timeline:

- T-10 BD (2 weeks) from planned send date: Issue must be opened by requester
- T-7 BD from planned send date: Copy due from requester (note: if copy is not provided by deadline, the send will be pushed out.)
- T-5 BD from planned send date: Test email sent to requester by MPM
- T-4 BD from planned send date: All requested changes from requester incorporated into email by MPM
- T-2 BD from planned send date: Email is set to send by MPM

**Add to nurture:**

Dependency: for any offline campaigns/tactics (i.e. event, sponsored webcast) _MOps list upload issue_ must be requested through an issue, aligning to MOps timeline must be requested as part of the campaign/tactic.

- T-10 BD (2 weeks) from planned follow up send date: Issue must be opened by requester
- T-7 BD from planned follow up send date: Requester notifies in the issue which nurture stream (and buyer stage) to add the leads to
- T+1 BD from planned follow up send date: campaign members added to the nurture stream indicated by requester, completed by MPM
    - If no follow up email indicated, leads can also be added to nurture T+1 BD from list upload

**Landing Pages:**

- T-15 BD (3 weeks) from planned launch date: Issue must be opened by requester
- T-10 BD (2 weeks) from planned launch date: Copy due from requester (if copy is not provided on time, launch will be pushed out)
- T-5 BD from planned launch date: Test landing page provided to requester by MPM

Note: complex requests (single landing page for multiple events, a page with a custom form, or email design that falls outside of a currently available template) will require additional time and MPM/MOps will discuss timeline upon review of the campaign (in Plan state).

## Marketing Programs project management

`Please note: this is being simplified and phased out. Details will be included in the project management section for Campaigns Team here:` https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#project-management

### Marketing Programs issues and issue templates

`Please note: this is being simplified and phased out. Details will be included in the project management section for Campaigns Team here:` https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#issue-templates

### Marketing Programs labels in GitLab

`Please note: this is being simplified and phased out. Details will be included in the project management section for Campaigns Team here:` https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#project-management

## List views of labels

`Please note: this is being simplified and phased out. Details will be included in the project management section for Campaigns Team here:` https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#project-management

### Tips & Tricks

#### Creating a MacBook shortcut for repetitive statements

`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#project-management](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#project-management)

# Reporting

## Integrated campaigns reporting
`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting)

### Key Metrics tracked on the Integrated campaign dashboards
`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting)


### 💡 Questions that the Integrated Campaign dashboards attempt to answer
#### Overall (WIP to deliver all)
`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting)

#### By Campaign
`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting)

#### Drilling down into the dashboards
`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting)


## Offer-Specific Dashboards
`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting)

### Key Metrics tracked on ALL virtual events dashboards
`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting)

#### Virtual Events Reporting
`The section below is being phased out. Please see here:` [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/#reporting)
