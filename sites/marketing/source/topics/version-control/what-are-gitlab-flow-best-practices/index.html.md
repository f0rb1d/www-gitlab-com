---
layout: markdown_page
title: "What are GitLab Flow best practices?"
description: "Using these best practices, software development teams can use GitLab Flow to software development."
---
 
When software development teams rush to accelerate delivery, they may end up with messy or complex workflows. Organizations that have transitioned from another version control system are especially likely to deal with challenging processes that may slow down development. When teams use GitLab Flow, they can use feature driven development and feature branches with issue tracking to ensure every team member works efficiently. Using these GitLab Flow tips, software development teams can simplify the process and produce a more efficient and cleaner outcome.
 
<i class="fas fa-code-branch" aria-hidden="true"></i> **1. Use feature branches rather than direct commits on the main branch.**
{: .alert .alert-success .green}
 
Using feature branches is a simple way to develop and keep the source code clean. If a team has recently transitioned to Git from SVN, for example, they’ll be used to a trunk-based workflow. When using Git, developers should create a branch for anything they’re working on so that contributors can easily start the code review process before merging.
 
<i class="fas fa-pencil-alt" aria-hidden="true"></i> **2. Test all commits, not only ones on the main branch.**
{: .alert .alert-success .green}
 
Some developers set up their CI to only test what has been merged into the `main` branch, but this is too late in the software development lifecyle, and everyone - from developers to product managers - should feel feel confident that the `main` branch always has green tests. It’s inefficient for developers to have to test `main` before they start developing new features. 
 
<i class="fas fa-flask" aria-hidden="true"></i> **3. Run every test on all commits. (If tests run longer than 5 minutes, they can run in parallel.)**
{: .alert .alert-success .green}
 
When working on a `feature` branch and adding new commits, run tests right away. If the tests are taking a long time, try running them in parallel. Do this server-side in merge requests, running the complete test suite. If there is a test suite for development and another only for new versions, it’s worthwhile to set up [parallel] tests and run them all.
 
<i class="fas fa-code" aria-hidden="true"></i> **4. Perform code reviews before merging into the main branch.**
{: .alert .alert-success .green}
 
Don't test everything at the end of a week or project. Code reviews should take place as soon as possible, because developers are more likely to identify issues that could cause problems later in the lifecycle. Since they’ll find problems earlier, they’ll have an easier time creating solutions.
 
<i class="fas fa-terminal" aria-hidden="true"></i> **5. Deployments are automatic based on branches or tags.**
{: .alert .alert-success .green}
 
If developers don't want to deploy `main` every time, they can create a `production` branch. Rather than using a script or doing it manually, teams can use automation or have a specific branch that triggers a [production deploy](https://docs.gitlab.com/ee/ci/yaml/README.html#environment).
 
<i class="fas fa-tags" aria-hidden="true"></i> **6. Tags are set by the user, not by CI.**
{: .alert .alert-success .green}
 
Developers should use `tags` so that the CI will perform an action rather than having the CI change the repository. If teams require detailed metrics, they should have a server report detailing new versions.
 
<i class="fas fa-cloud-upload-alt" aria-hidden="true"></i> **7. Releases are based on tags.**
{: .alert .alert-success .green}
 
Each tag should create a new release. This practice ensures a clean, efficient development environment.
 
<i class="fas fa-eye-slash" aria-hidden="true"></i> **8. Pushed commits are never rebased.**
{: .alert .alert-success .green}
 
When pushing to a public branch, developers shouldn't rebase it, because that makes it difficult to identify the improvement and test results, while <a href="https://git-scm.com/docs/git-cherry-pick" target="_blank">cherry picking</a>. Sometimes this tip can be ignored when asking someone to squash and rebase at the end of a code review process to make something easier to revert. However, in general, the guideline is: Code should be clean, and history should be realistic.
 
<i class="far fa-folder-open" aria-hidden="true"></i> **9. Everyone starts from main and targets main.**
{: .alert .alert-success .green}
 
This tip prevents long branches. Developers check out `main`, build a feature, create a merge request, and target `main` again. They should do a complete review **before** merging and eliminating any intermediate stages.
 
<i class="fas fa-bug" aria-hidden="true"></i> **10. Fix bugs in main first and release branches second.**
{: .alert .alert-success .green}
 
After identifying a bug, a problematic action someone could take is fix it in the just-released version and not fix it in `main`. To avoid it, developers should always fix forward by pushing the change in `main`, then cherry-pick it into another `patch-release` branch.
 
<i class="far fa-edit" aria-hidden="true"></i> **11. Commit messages reflect intent.**
{: .alert .alert-success .green}
 
Developers should not only say what they did, but also why they did it. An even more useful tactic is to explain why this option was selected over others to help future contributors understand the development process. Writing descriptive commit messages is useful for code reviews and future development.
 
## Want to learn more about software development best practices?
 
* [Learn how GitLab modernizes software development](/solutions/version-control/)
* [Watch how GitLab enhances developer collaboration and delivery](/webcast/collaboration-without-boundaries/)
* [Download the version control best practices eBook to accelerate delivery](/resources/ebook-version-control-best-practices/)
 
<style>
.green {
 color: rgb(60,118,61) !important;
}
.green i {
 color: rgb(226,67,41) !important;
}
</style>
